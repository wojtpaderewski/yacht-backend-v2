import { Controller, Post, Route, Body, Res, TsoaResponse, Get} from 'tsoa';
import { Rent, RentInterface, RentAddInterface } from '../db/models/rent.model';
import { Yacht } from '../db/models/yacht.model';
import { Equipment } from '../db/models/equipment.model';
import { Client } from '../db/models/client.model';

@Route('/rent')
export class rent extends Controller {
    @Post()
    public async start(@Body() body: RentAddInterface, @Res() validationError: TsoaResponse<400, string> ): Promise<RentInterface | null> {
        try {
            const yacht = await Yacht.findOne({ where: {id: body.yachtId}, include: [Equipment, Rent]});
            const client = await Client.findOne({ where: {id: body.clientId}});

            if (!yacht || !client) {
                return null;
            }

            let isRented = false;
            let validateDateStart = new Date();
            let validateDateEnd = new Date();

            yacht.rents.forEach(rent => {
                if (body.rent_start > rent.rent_end || body.rent_end > rent.rent_start) {
                    isRented = true;
                    validateDateStart = rent.rent_start;
                    validateDateEnd = rent.rent_end;
                }
            });

            if (isRented) {
                return validationError(400, `Jacht jest w wypożyczeniu w terminie ${validateDateStart.toLocaleDateString()} do ${validateDateEnd.toLocaleDateString()}`);
            }
            
            const newRent = new Rent({ rent_start: body.rent_start, rent_end: body.rent_end, fullPrice: body.fullPrice});
            newRent.yachtId = body.yachtId;
            newRent.clientId = body.clientId;

            await newRent.save();

            client.rentId = newRent.id;

            await yacht.save();
            await client.save();

            return newRent;
        } catch (error) {
            console.log(error);
            return null;
        }
    }

    @Get()
    public async getAll(): Promise<RentInterface []> {
        const rents = await Rent.findAll({include: [Client, Yacht]});
        return rents;
    }
}