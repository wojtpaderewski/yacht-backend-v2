import { Controller, Get, Route, Path, Put, Body} from 'tsoa';
import { Yacht, YachtInterface } from '../db/models/yacht.model';
import { Equipment } from '../db/models/equipment.model';

@Route('/yacht')
export class yacht extends Controller {
    @Get()
    public async getAll(): Promise<YachtInterface[]> {
        const yachts = await Yacht.findAll({include: Equipment});
        return yachts;
    }

    @Get('{id}')
    public async getById(@Path() id: number ): Promise<YachtInterface | null> {
        const yacht = await Yacht.findOne({ where: {id}, include: Equipment});
        return yacht;
    }
}