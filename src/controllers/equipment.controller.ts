import { Controller, Get, Route, Path, Put, Res, TsoaResponse} from 'tsoa';
import { Yacht, YachtInterface } from '../db/models/yacht.model';
import { Equipment, EquipmentInterface } from '../db/models/equipment.model';

@Route('/equipment')
export class equipment extends Controller {
    @Get()
    public async getAll(): Promise<EquipmentInterface[]> {
        const equipments = await Equipment.findAll();
        return equipments;
    }
}