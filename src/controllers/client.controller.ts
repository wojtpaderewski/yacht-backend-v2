import { Controller, Post, Route, Body, Res, TsoaResponse, Get, Path} from 'tsoa';
import { Client, ClientAddInterface, ClientInterface } from '../db/models/client.model';

@Route('/client')
export class client extends Controller {
    @Post()
    public async add(@Body() body: ClientAddInterface, @Res() validationError: TsoaResponse<400, unknown> ): Promise<ClientInterface | null> {
        try {
            const client = await Client.findOne({ where: {name: body.name, email: body.email, surname: body.surname}});
            if (client) { // validtion from adding to much clients with this same data
                return client;
            }

            const newClient = new Client({name: body.name, surname: body.surname, email: body.email, isClubMember: body.isClubMember });
            await newClient.save();

            return newClient;
        } catch (error) {
            console.log(error);
            return null;
        }
    }

    @Get('{id}')
    public async getById(@Path() id: number ): Promise<ClientInterface | null> {
        const yacht = await Client.findOne();
        return yacht;
    }
}