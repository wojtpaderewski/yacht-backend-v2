import { logger } from './logger';
import { Request, Response, NextFunction } from 'express';

const requestLoggerMiddleware = (req: Request, res: Response, next: NextFunction): void => {
    logger.http(`[${req.method}] ${req.originalUrl}`, req.body);
    
    const start = new Date().getTime();
    res.on('finish', () => {
        const elapsed = new Date().getTime() - start;
        logger.http(`[${req.method}] ${req.originalUrl}: finished with code ${res.statusCode}, elapsed: ${elapsed}ms`);
    });
    next();
};

export { requestLoggerMiddleware };
