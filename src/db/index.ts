import { Sequelize } from 'sequelize-typescript';
import { logger } from '../logger';
const config = require('dotenv').config({path: __dirname + '/../../.env'}).parsed;

if (!config) {
    logger.error('db/index: .env file parsing error');
}

const sequelize = new Sequelize({
    database: config.DB_NAME,
    dialect: 'postgres',
    username: config.DB_USER,
    password: config.DB_PASSWORD,
    host: config.DB_HOST,
    logging: false,
    models: [__dirname + '/models/**/*.model.ts'],
    modelMatch: (filename, member) => {
        return filename.substring(0, filename.indexOf('.model')) === member.toLowerCase();
    }
});

export default sequelize;