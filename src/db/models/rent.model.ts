import { Table, Column, Model, HasMany, Unique, NotEmpty, ForeignKey, BelongsTo, DataType } from 'sequelize-typescript';
import { Yacht, YachtInterface } from './yacht.model';
import { Client, ClientInterface } from './client.model';

export interface RentInterface {
    id:number;
    rent_start:Date;
    rent_end:Date;
    fullPrice:number;
    yacht: YachtInterface;
    client: ClientInterface;
}

export interface RentAddInterface {
    rent_start:Date;
    rent_end:Date;
    yachtId: number;
    clientId: number;
    fullPrice:number;
}

@Table
export class Rent extends Model implements RentInterface {
    id: number

    @Column
    rent_start: Date;

    @Column
    rent_end: Date;

    @Column(DataType.FLOAT)
    fullPrice: number;

    @ForeignKey(() => Yacht)
    @Column
    yachtId: number;

    @BelongsTo(() => Yacht)
    yacht: Yacht;

    @ForeignKey(() => Client)
    @Column
    clientId: number;

    @BelongsTo(() => Client)
    client: Client;
}