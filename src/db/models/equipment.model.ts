import { Table, Column, Model, HasMany, Unique, NotEmpty, ForeignKey, BelongsTo, DataType, Default } from 'sequelize-typescript';
import { Yacht, YachtInterface } from './yacht.model';

export interface EquipmentInterface {
    id:number;
    name:string;
    price:number;
    yachtId: number;
    yacht: YachtInterface;
}

@Table
export class Equipment extends Model implements EquipmentInterface {
    id: number

    @NotEmpty
    @Column
    name: string;

    @NotEmpty
    @Column(DataType.FLOAT)
    price: number;

    @ForeignKey(() => Yacht)
    @Column
    yachtId: number;

    @BelongsTo(() => Yacht)
    yacht: Yacht;
}