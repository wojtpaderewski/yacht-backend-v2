import { Table, Column, Model, BelongsTo, Unique, NotNull, ForeignKey, AllowNull, NotEmpty, Default } from 'sequelize-typescript';
import { Rent, RentInterface } from './rent.model';
import { Yacht, YachtInterface } from './yacht.model';

export interface ClientInterface {
    id: number;
    name: string;
    surname: string;
    email: string;
    isClubMember: boolean;
    rentId: number;
    rent: RentInterface;
}

export interface ClientAddInterface {
    name: string;
    surname: string;
    email: string;
    isClubMember: boolean;
}

@Table
export class Client extends Model implements ClientInterface {
    id: number

    @NotEmpty
    @Column
    name: string;

    @NotEmpty
    @Column
    surname: string;

    @NotEmpty
    @Column
    email: string;

    @Default(false)
    @Column
    isClubMember: boolean;

    @ForeignKey(() => Rent)
    @Column
    rentId: number;

    @BelongsTo(() => Rent)
    rent: Rent;
}

