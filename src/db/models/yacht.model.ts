import { Table, Column, Model, HasMany, Unique, NotEmpty, DataType, BelongsTo, ForeignKey, Default } from 'sequelize-typescript';
import { Equipment, EquipmentInterface } from './equipment.model';
import { Rent, RentInterface } from './rent.model';

export interface YachtInterface {
    id:number;
    name:string;
    year:string;
    length: number;
    width:number;
    priceForDay: number;
    quantity:number;
    rents: RentInterface [];
    equipments:EquipmentInterface[];
}

@Table
export class Yacht extends Model implements YachtInterface {
    id: number

    @NotEmpty
    @Unique
    @Column
    name: string;

    @NotEmpty
    @Column
    year: string;
    
    @NotEmpty
    @Column(DataType.FLOAT)
    length: number;

    @NotEmpty
    @Column(DataType.FLOAT)
    width: number;

    @NotEmpty
    @Column(DataType.FLOAT)
    priceForDay: number;

    @NotEmpty
    @Column
    quantity: number;

    @HasMany(() => Rent)
    rents: Rent [];

    @HasMany(() => Equipment)
    equipments: Equipment[];

}