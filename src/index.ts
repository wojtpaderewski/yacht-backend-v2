import express from 'express';
import { logger } from './logger';
import { requestLoggerMiddleware } from './request.logger.middleware';
import { RegisterRoutes } from './routes';
import db from './db';
import session from 'express-session';
import path from 'path';
import cors from 'cors';

const port = 3000;

const app = express();
app.use(express.static('public'));
app.use(express.json());
app.use(requestLoggerMiddleware);

app.use(cors({
    credentials: true,
    origin: 'http://localhost:8080',
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE'
}));

app.use(session({
    secret: 'mn949kdg51fCad151546vawvdvdyudf151f48',
    resave: false,
    saveUninitialized: true,
    cookie: {
        maxAge: 1000 * 60 * 60 * 24
    }
}));

app.use('/swagger.json', (req, res) => {
    res.sendFile(path.join(__dirname, '..', 'swagger.json'));
});

RegisterRoutes(app);
db.sync();

app.listen(port, () => {
    logger.info(`app started at http://localhost:${port}`);
});


export default app;
