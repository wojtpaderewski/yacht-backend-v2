
import db from './db';
import { Yacht } from './db/models/yacht.model';
import { logger } from './logger';
import { Equipment } from './db/models/equipment.model';

const populate = async () => {
    try {
        await db.sync({
            force: true
        });

        const antila_27  = new Yacht({ name: 'Antila 27', year: 2018, priceForDay: 450, length: 8.24, width: 2.98, quantity: 8});
        const antila_22  = new Yacht({ name: 'Antila 22', year: 2014, priceForDay: 350, length: 6.68, width: 2.68, quantity: 6});
        const sasanka_620  = new Yacht({ name: 'Sasanka 620', year: 2008, priceForDay: 250, length: 6.2, width: 2.5, quantity: 4});
    
        await antila_27.save();
        await sasanka_620.save();
        await antila_22.save();
            
        const fridge = new Equipment({name: 'Lodówka turystczna', price: 150});
        const toilet = new Equipment({name: 'Toleta', price: 150});
        const fridge1 = new Equipment({name: 'Lodówka', price: 100});
        const toilet2 = new Equipment({name: 'Toleta', price: 150});
        const grill = new Equipment({name: 'Grill', price: 150});

        fridge.yachtId = antila_27.id;
        toilet.yachtId = antila_27.id;

        fridge1.yachtId = antila_22.id;
        grill.yachtId = antila_22.id;

        toilet2.yachtId = sasanka_620.id;
    
        await fridge.save();
        await toilet.save();
        await fridge1.save();
        await toilet2.save();
        await grill.save();
    
        logger.info('Database popultion success');
    } catch (error) {
        console.log(error);
        
    }
};

populate().then(() => {
    db.close();
});